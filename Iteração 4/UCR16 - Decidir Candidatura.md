# Diagrama de Responsabilidades #

![DiagramaResponsabilidade16.png](https://bitbucket.org/repo/4aRMAx/images/3298211123-DiagramaResponsabilidade16.png)

# Diagrama de Sequência #

![ds16.jpg](https://bitbucket.org/repo/4aRMAx/images/4070143411-ds16.jpg)
![ds16_1.jpg](https://bitbucket.org/repo/4aRMAx/images/1263851597-ds16_1.jpg)
![ds16_2.jpg](https://bitbucket.org/repo/4aRMAx/images/1527504244-ds16_2.jpg)

# Diagrama de Classes #

![DS16.jpg](https://bitbucket.org/repo/4aRMAx/images/1889952002-DS16.jpg)