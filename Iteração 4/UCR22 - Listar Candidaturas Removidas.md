# Realização do UC22 Carregar Dados Exposicao Anterior #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “CarregaDadosExposicaoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------


# Diagrama de Sequencia #
![ds 22 Finalissiomo done.jpg](https://bitbucket.org/repo/4aRMAx/images/1110693786-ds%2022%20Finalissiomo%20done.jpg)


# Diagrama de Classes #

![dc22.jpg](https://bitbucket.org/repo/4aRMAx/images/892770386-dc22.jpg)