# Tabela de Responsabilidades #

![4.JPG](https://bitbucket.org/repo/4aRMAx/images/2620121293-4.JPG)

# Diagrama de Sequência #

![DS4.jpg](https://bitbucket.org/repo/4aRMAx/images/1518544513-DS4.jpg)

## Diagrama de Exposições do FAE ##

![Exposições FAE.jpg](https://bitbucket.org/repo/4aRMAx/images/3295590672-Exposi%C3%A7%C3%B5es%20FAE.jpg)

## Diagrama de Candidaturas Atribuídas ao FAE ##

![Candidaturas FAE.jpg](https://bitbucket.org/repo/4aRMAx/images/2627728243-Candidaturas%20FAE.jpg)

## Diagrama de alteração de Estado de Candidatura ##

![Candidatura Avaliada.jpg](https://bitbucket.org/repo/4aRMAx/images/1295253383-Candidatura%20Avaliada.jpg)

# Diagrama de Classes #

![DC4.jpg](https://bitbucket.org/repo/4aRMAx/images/3919569136-DC4.jpg)
