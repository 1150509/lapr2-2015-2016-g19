# Tabela de Responsabilidades UC19 - Remover Candidatura #

Fluxo Principal | Pergunta | Resposta | Justificação
---|:---:|:---:|---
1. O organizador inicia no sistema a remoção de uma candidatura. | n/a | | 
 | |
2. O sistema apresenta uma lista de exposições e solicita a escolha de uma delas. | Quem tem os Dados das Exposições | RegistoExposições | Regra Creator | IE: no DC RegistoExposições tem várias Exposições
 | |
3. O Organizador seleciona uma exposição | Quem guarda a seleção? | RemoverCandidaturaController| O Controller é responsável pela interação entre o utilizador e o sistema.
 | |
4. O Sistema apresenta uma lista de candidaturas e solicita a seleção de uma delas. | Qutem tem os dados das Candidaturas |RegistoCandidaturas | IE: no DC RegistoCandidaturas tem várias Candidaturas
 | |
5. O Organizador seleciona uma candidatura. | Quem guarda a candidatura selecionada? |	RemoverCandidaturaController |	O Controller é responsável pela interação entre o utilizador e o sistema
 | |
6. O Sistema remove a candidatura selecionada. | Onde se Encontra a candidatura	| List<Candidatura> | Existe um número de candidaturas por exposição e por isso elas são guardadas num vetor numa classe responsável por lidar com os metodos ligados a esse vetor.

___

# SD UC19 - Remover Candidatura #

![UC 19 SD It4.jpg](https://bitbucket.org/repo/4aRMAx/images/3786985770-UC%2019%20SD%20It4.jpg)

# SD SetFaeDefinido #

![SetFaeDefinido.jpg](https://bitbucket.org/repo/4aRMAx/images/3984296977-SetFaeDefinido.jpg)

# CD UC 19- Remover Candidatura #

![DC IT4- UC 19.jpg](https://bitbucket.org/repo/4aRMAx/images/33209787-DC%20IT4-%20UC%2019.jpg)