## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “DefinirRecursoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**
![UC10-Racional.png](https://bitbucket.org/repo/BKrx4b/images/482656447-UC10-Racional.png)

## Diagrama de Sequência ##
![DS 10.jpg](https://bitbucket.org/repo/4aRMAx/images/1817136674-DS%2010.jpg)
## Diagrama de Classes ##
![dc10.jpg](https://bitbucket.org/repo/BKrx4b/images/4216266833-dc10.jpg)