# Realização do UC3 Atribuir Candidatura #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “AtribuirCandidaturaController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O organizador inicia no sistema a atribuição de candidaturas. |  permite obter as exposições do organizador? |CentroExposicoes, RegistoExposicoes, Exposição, ListaOrganizadores, Organizador| IE, HC, LC
2.  O sistema mostra as exposições ativas para o organizador e solicita escolha de uma. | ... conhece todas as exposições? | RegistoExposicoes | IE
3. O organizador seleciona uma exposição.|... permite obter mecanismos de atribuição |CentroExposicoes, RegistoMecanismos |IE, HC, LC
4. O sistema mostra os mecanismos de atribuição disponíveis e solicita a escolha de um. |tem a lista de mecanismos?|CentroExposicoes | IE: Centro de Exposições tem conhecimento de todos os mecanismos
5.  O organizador seleciona um mecanismo de atribuição. |... executa a distribuição das candidaturas consoante o mecanismo escolhido |MecanismoAtribuicao,Exposição,ListaCandidaturas, ListaFAEs, ListaAtribuicoes, Atribuicao|IE, HC, LC, Creator.
6. O sistema mostra as associações entre os FAE e as candidaturas.|||
7. O organizador confirma.|||
8. O sistema regista as atribuições e informa o sucesso da operação.|regista as atribuições?| ListaAtribuicoes| Information Expert 

## Diagrama de Sequencia ##
![UC3-It2-DS.jpg](https://bitbucket.org/repo/BKrx4b/images/688178346-UC3-It2-DS.jpg)
## Diagrama de Classes ##
![dc3.png](https://bitbucket.org/repo/BKrx4b/images/4029116593-dc3.png)