# Realização do UC8 Criar Demonstraçao #

## Racional ##

**Quem coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “CriarDemonstracaoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O organizador inicia o caso de uso para criar demonstração.
2. O sistema pede a introdução dos dados.
3. O organizador introduz os dados.|guarda os dados de demonstração?|Demonstração|IE: A classe Demonstração contém os atributos necessários para guardar os dados.
4.O sistema valida os dados e pede confirmação.|… quem valida os dados?|Demonstração\Exposição|Demonstração-Validação local-Exposição-Validação global
5.O organizador confirma os dados.
6.O sistema guarda a demonstração e informa o Organizador o sucesso da operação.|… guarda a demonstração da Exposição?|Exposição.|IE: Exposição guarda todos os aspetos de uma exposição.

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:


* Centro de Exposições  
* Exposição  
* Demonstracao  
* Recurso

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* CriarDemonstracaoUI  
* CriarDemonstracaoController 
* listDemonstracoes
## Diagrama de Sequência ##
![SD-It2-UC8.jpg](https://bitbucket.org/repo/4aRMAx/images/1998799382-SD-It2-UC8.jpg)
Interaction Use
![getExposicoesOrganizador.jpg](https://bitbucket.org/repo/BKrx4b/images/1700063214-getExposicoesOrganizador.jpg)
![SetDemonstracaoCriada.jpg](https://bitbucket.org/repo/BKrx4b/images/1701968905-SetDemonstracaoCriada.jpg)
![SetDemonstracaoCriada2().jpg](https://bitbucket.org/repo/BKrx4b/images/2581057958-SetDemonstracaoCriada2().jpg)
![SetEstadoDemonstracaoCriada.jpg](https://bitbucket.org/repo/4aRMAx/images/138977385-SetEstadoDemonstracaoCriada.jpg)
## Diagrama de Classes ##
![dc8.jpg](https://bitbucket.org/repo/BKrx4b/images/1827751342-dc8.jpg)
