# Tabela de Responsabilidades #

![18.JPG](https://bitbucket.org/repo/4aRMAx/images/1110365877-18.JPG)

# Diagrama de Sequência #

![DS18.jpg](https://bitbucket.org/repo/4aRMAx/images/342403157-DS18.jpg)

## Diagrama de Candidaturas do Representante com Stand por confirmar ##

![Candidaturas Representante.jpg](https://bitbucket.org/repo/4aRMAx/images/1427274576-Candidaturas%20Representante.jpg)

## Diagrama de procurar Atribuição de Stand ##

![AtribuicaoStand.jpg](https://bitbucket.org/repo/4aRMAx/images/2675855305-AtribuicaoStand.jpg)

## Diagrama de alteração de Estado para Confirmado ##

![Stand Confirmado.jpg](https://bitbucket.org/repo/4aRMAx/images/448502244-Stand%20Confirmado.jpg)

# Diagrama de Classes #

![DC18.jpg](https://bitbucket.org/repo/4aRMAx/images/3818164392-DC18.jpg)
