## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “DefinirFAEController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O organizador inicia no sistema a definição dos FAE. |... contém exposições a serem alteradas? | RegistoExposicoes |IE: RegistoExposicoes regista exposições. Pode filtrar as exposições registados para o utilizador em causa.
2. O sistema mostra a lista das exposições, nas quais utilizador é organizador. | n/a | | 
3. O organizador seleciona a exposição pretendida. | ... obtém o objeto exposição através do seu identificador? |Centro de Exposições |	IE: Centro Exposições regista Exposições. 
4. O sistema solicita a identificação de um utilizador para a membro da FAE. |n/a		
5. O organizador introduz o identificador do novo membro. |... obtém o utilizador através do identificador respetivo?|	RegistoUtilizadores|IE: RegistoUtilizadores conhece todos os utilizadores e por conseguinte todos os seus identificadores. O resultado será um  objeto Utilizador.
| ... cria o objeto FAE?|	Exposição|	Creator: Regra1
| ... associa o objeto Utilizador ao objeto FAE?|	Exposição|	IE: Exposição conhece/tem o objeto FAE.
6. O sistema valida e solicita confirmação.|	... valida os dados do FAE?|	FAE|	IE: Validação local.
		||Exposição|	IE: Validação global.
7. O organizador confirma os dados.|	n/a	||
8. O sistema regista o novo membro.|	... guarda os dados do FAE?|	Exposição|	IE: no MD a Exposição tem vários FAE
Os passos 4-8 são repetidos até a lista de FAE estar completa.|	n/a	||
10. O Sistema atribui os FAE à exposição e informa o organizador o sucesso da operação.|	... faz a atribuição?|	Exposição|	IE: no MD a exposição  tem FAE.		


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:


* Centro de Exposições  
* Exposição  
* FAE
* Utilizador  

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* DefinirFAEUI  
* DefinirFAEController 
* ListaFAE
* RegistoUtilizador
* RegistoExposicoes
* ListOrganizadores
## Diagrama de Sequência ##
![UC2-DS-IT2.jpg](https://bitbucket.org/repo/4aRMAx/images/1934567848-UC2-DS-IT2.jpg)

![DC2 - State2.jpg](https://bitbucket.org/repo/4aRMAx/images/3016704077-DC2%20-%20State2.jpg)

![UC2-SSD.jpg](https://bitbucket.org/repo/4aRMAx/images/3396185745-UC2-SSD.jpg)

## Diagrama de Sequência ##

![DC2.png](https://bitbucket.org/repo/4aRMAx/images/3412230390-DC2.png)