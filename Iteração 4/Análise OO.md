# Análise OO #

## Racional para identificação de classes ##
Categorias | Conceito de Negócio
---|---:
 | |
Transações (do negócio): | Candidatura de demonstração; Stand;
 | |
Linhas de Transações: | 
 | |
Produto ou serviços relacionados com as transações: | Produto
 | |
Registos (de transações): | 
 | |
Papéis das pessoas: | Gestor de Exposições; Organizador; FAE; Utilizador; Representante de expositor
 | |
Lugares: | Centro de Exposição
 | |
Eventos: | Exposição
 | | 
Objetos Físicos: | 
 | | 
Especificações e Descrições: | Mecanismos de atribuição; Mecanismos de deteção de conflitos
 | |
Catálogos: | 
 | |
Conjuntos: | Lista de Demonstrações; Lista de Recursos; Lista de Stands;
 | |
Elementos de conjuntos: |
 | |
Organizações: | Expositor
 | |
Outros sistemas (externos): | 
 | |
Registos (financeiros), de trabalho, contractos, documentos legais: | 
 | |
Instrumentos financeiros: | 
 | |
Documentos referidos/para executar as tarefas: | 

___


## Tabela de Associações ##
Conceito A | Associação | Conceito B
---|:---:|---:
 | | 
Gestor de exposições | cria | exposição
                    | define | organizadores
                    | confirma registo de | utilizador
                    | é | utilizador
                    | define | recursos
                    | define | tipos de conflitos
                    | cria | stands
                    | cria | ficheiro de estatística
 | | 
FAE | avalia | candidatura
    | é | utilizador
    | atualiza | conflitos de interesse
 | | 
Representante do expositor | cria | candidatura a exposição
                           | é | utilizador
                           | altera | candidatura
                           | cria | candidatura de demonstração
                           | confirma | interesse nos stands
                           | remove | candidatura
 | | 
Organizador | define | FAE
            | organiza | exposição
            | atribui | candidaturas
            | é | utilizador
            | cria | demonstração
            | decide | demonstração
            | decide | candidatura de exposição
            | atribui | stands
            | aceita | candidatura de demonstração
            | carrega | dados das exposições anteriores
            | lista | candidaturas removidas
            | analisa | qualidade de FAE
 | | 
Exposição | é criada por | gestor de exposições
          | tem | organizadores
          | tem | FAE
          | tem | demonstrações
          | está contida no | centro de exposições
          | tem | conflitos
 | | 
Candidatura de exposição | é avaliada por | FAE
                         | é criada pelo | representante do expositor
                         | é atribuída pelo | organizador
                         | é alterada pelo | representante do expositor
                         | é decidida pelo | organizador
                         | é atribuída a | FAE
 | | 
Candidatura de demonstração | é criada pelo | representante do expositor
                            | é aceite pelo | organizador
 | | 
Avaliação | feita por | FAE
 | | 
Utilizador | é confirmado por | gestor de exposições
           | faz parte | centro de exposição
           | pode ser | representante do expositor
           | pode ser | FAE
           | pode ser | organizador
           | pode ser | gestor de exposições
           | altera | perfil de utilizador
           | faz | login
 | | 
Centro de exposição | tem registo de | utilizador
                    | tem registo de | exposição
                    | tem registo de | recurso
                    | tem registo de | tipos de conflito
 | |
Stand | é criado por | gestor de exposições
      | é atribuído por | organizador
      | é atribuído a | candidatura
 | |
Demonstração | é criada por | organizador
             | é decidida por | organizador
 | |
Ficheiro de Estatística | é criado por | gestor de exposições
 | |
Conflito | é atualizado por | FAE
         | está contido em | Exposição
         | tem | tipo de conflito
 | |
Tipo de Conflito | é definido por | gestor de exposições
                 | está contido em | centro de exposição
 | |
Atribuição | é referente a | candidatura de exposição
           | é referente a | FAE
           | é feita por | organizador

___


# Modelo de Domínio #

![ModeloDominio-It4.jpg](https://bitbucket.org/repo/4aRMAx/images/2662869801-ModeloDominio-It4.jpg)

# Diagrama de Estados #

## Exposição: ##

![State diagram v2.jpg](https://bitbucket.org/repo/4aRMAx/images/371426657-State%20diagram%20v2.jpg)

## Candidatura: ##

![DE_Demo.jpg](https://bitbucket.org/repo/4aRMAx/images/323699096-DE_Demo.jpg)

## Demonstração: ##

![DiagramaEstadosDemonstração.jpg](https://bitbucket.org/repo/4aRMAx/images/2068398167-DiagramaEstadosDemonstra%C3%A7%C3%A3o.jpg)