## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “DefinirFAEController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1-O gestor Inicia o caso de uso de criar o ficheiro de estatística Comma-Separated Value (CSV).|na||
2-O sistema mostra uma lista de exposições e pede a seleção de uma.|tem a lista de exposicoes|RegistoExposicoes|IE
3-O gestor seleciona uma exposição.|na||
4-O sistema cria o ficheiro e informa o gestor do sucesso da operação.|cria o ficheiro|FicheiroCSV|IE

# Diagrama de Sequencia #
![DS UC 23.jpg](https://bitbucket.org/repo/4aRMAx/images/2564862381-DS%20UC%2023.jpg)


# Diagrama de Classe#

![dc23.jpg](https://bitbucket.org/repo/4aRMAx/images/1466225044-dc23.jpg)