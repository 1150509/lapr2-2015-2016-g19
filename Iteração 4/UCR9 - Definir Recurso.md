# Realização do UC9 Definir Recurso #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “DefinirRecursoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**


Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1.O gestor de exposições inicia o processo de definir recursos.|cria instância de recurso?|Centro de Exposições|Creator: Regra 1
2.O sistema pede a introdução dos dados sobre os recursos necessários para a exposição.
3.O gestor introduz os dados.|… guarda os dados de recurso?|Recurso|Information Expert
4.O sistema valida os dados e pede confirmação dos dados.|… valida os dados ?|Centro de Exposições|Validação global (e.g. duplicados)
5.O gestor confirma dados.
6.O sistema guarda os recursos e informa o gestor de exposições do sucesso da operação.|Onde se guarda os recursos?|Centro de Exposições|IE: Exposição vai guardar todos os aspetos da Exposição.

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:


* Centro de Exposições  
* Recurso

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* DefinirRecursosUI  
* DefinirRecursosController 
* RegistoDeRecursos
## Diagrama de Sequência ##
![ds9.png](https://bitbucket.org/repo/BKrx4b/images/308016181-ds9.png)
## Diagrama de Classes ##
![dc9.png](https://bitbucket.org/repo/BKrx4b/images/3964589026-dc9.png)