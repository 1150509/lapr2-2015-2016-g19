# UC 13- Detetar Conflito #

## Formato Breve ##

Quando chega a data final o TEMPO procura conflitos. O TEMPO encontra um conflito. O TEMPO guarda a informação.

## SSD Formato Breve ##
![UC13 - SSD.jpg](https://bitbucket.org/repo/BKrx4b/images/584332772-UC13%20-%20SSD.jpg)
## Formato Completo 

**Ator principal:** 

TEMPO

**Partes interessadas e seus interesses:**

Gestor De Exposições,Representante do Expositor,FAE,Organizador

**Pré-condições** 

A data final ter chegado.

**Pós-Condições** 

O conflito ter sido guardado.

**Cenário de sucesso principal (ou fluxo básico)** 

1- Chega a data final, o que inicia automaticamente a funcionalidade de detetar conflitos.

2- O sistema procura conflitos.

3- O sistema encontra um conflito.

4- O sistema guarda o conflito.


**Extensões**

 *a. Um utilizador solicita o cancelamento da operação. O caso de uso termina.

**Requisitos especiais**

## Lista de Variações de Tecnologias e Dados ##
-
## Frequência de Ocorrência ##
-

## Questões em aberto ##
-