 UC19 - Remover Candidatura #

## Formato Breve ##

O Representante inicia a funcionalidade de Remover Candidaturas. O Sistema apresenta uma lista de exposições onde o Representante criou candidaturas e solicita a escolha de uma delas. O Representante escolhe uma exposição. O Sistema solicita confirmação da escolha. O Representante confirma a sua escolha. O  sistema apresenta uma lista de candidaturas dessa exposição, cujo criador foi o Representante que está a utilizar a aplicação e solicita a escolha de uma delas para remoção.O Representante escolhe uma delas.O Sistema solicita confirmação da escolha. O Representante confirma a sua escolha. O Sistema remove a candidatura selecionada e informa o Representante do sucesso da operação.


## SSD ##

![SSD-It4-UC19.jpg](https://bitbucket.org/repo/4aRMAx/images/4011146304-SSD-It4-UC19.jpg)

## Formato Completo ##

#### Ator principal ####

* Representante

#### Partes interessadas e seus interesses ####

* Representante 

#### Pré-condições ####

* Utilizador atutenticado no sistema como representante.
* Haver candidaturas criadas no sistema, cujo periodo de submissão ainda não tenha terminado.

### Pós-condições ###

* A remoção de candidaturas ter sido bem sucedida. 

### Cenário de sucesso principal (ou fluxo básico) ###
 
1. O Representante inicia a funcionalidade de Remover Candidaturas.
2. O Sistema apresenta uma lista de exposições e solicita a escolha de uma delas.
3. O Representante escolhe uma Exposição.
4. O Sistema verifica se a exposição contem pelo menos uma candidatura na qual o Representante que está a utilizar a aplicação foi responsável pela sua criação e cujo seu periodo de submissão não tenha terminado e solicita a confirmação da escolha.
5. O Representante confirma a sua escolha.
6. O Sistema apresenta uma lista de candidaturas cujo criador foi o Representante que está a utilizar a aplicação e cujo periodo de submissão ainda não tenha terminado e solicita a escolha de uma delas para remoção.
7. O Representante escolhe uma candidatura.
8. O Sistema solicita confirmação da sua escolha.
9. O Representante confirma a sua escolha.
10. O Sistema remove a candidatura escolhida.
11. Os passos 2 a 10 repetem-se enquanto o Representante assim o desejar.
12. O Sistema informa do sucesso da operação.

#### Extensões (ou fluxos alternativos) ####

*a. O gestor de exposições solicita o cancelamento da operação.
> O caso de uso termina.

4b. A Exposição selecionada não tem nenhuma candidatura cujo periodo de submissão ainda não tenha terminado
> 1. O sistema informa o Gestor de Exposições desse facto.
> 2. O Sistema permite a escolha de outra Exposição (Passo 2 ).

#### Requisitos especiais ####

* 

#### Lista de Variações de Tecnologias e Dados ####

* 

#### Frequência de Ocorrência ####

* 

#### Questões em aberto ####

* O Representante pode criar mais do que uma candidatura por exposição e, em mais do que uma exposição?