# Formato breve #

Gestor inicia o caso de uso de definir recursos. Sistema solicita os recursos. Gestor introduz os recursos. Sistema valida e pede confirmação. Utilizador confirma. Sistema regista os dados e informa do sucesso da operação.

## SSD de formato breve ##

![SSD9.jpg](https://bitbucket.org/repo/BKrx4b/images/520690007-SSD9.jpg)

# Formato completo #

## Ator principal ##

Gestor de exposições

## Partes interessadas e seus interesses ##

Gestor de exposições pretende definir os recursos disponíveis.
Centro de exposições pretende saber os recursos existentes para manter o seu registo e gerir na criação de demonstrações.

## Pré-condições ##

Gestor ser utilizador registado.

## Pós-condições ##

Recursos da exposição definidos.

## Cenário de sucesso principal (ou fluxo básico) ##

1. Utilizador inicia a funcionalidade de definição de recursos.
2. Sistema solicita a definição de recursos.
3. Utilizador introduz os recursos.
4. Sistema valida os dados e pede confirmação.
5. Utilizador confirma os dados.
6. Sistema regista os dados e informa do sucesso da operação.

## Fluxo alternativo ##

## Extensões (ou fluxos alternativos) ##
*a. O gestor de exposições solicita o cancelamento da operação.
> O caso de uso termina.
4a. O gestor não introduz o recurso.
> 1. O sistema informa o Gestor de Exposições desse facto e pede novamente a introdução.
> 2. O Gestor não introduz,o caso de uso termina.
-

## Requisitos especiais ##

-

## Lista de variações em tecnologias e dados ##
-

## Frequência de ocorrência ##

Enquanto existirem exposições sem os recursos disponíveis definidos.

## Questões em aberto ##

