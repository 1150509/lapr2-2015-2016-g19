# Formato Breve #
O gestor de exposições inicia o caso de uso de carregar dados de uma exposição anterior.O sistema solicita a escolha do ficheiro com a informação da exposição que o gestor pretende importar.O gestor escolhe o ficheiro.O sistema informa o gestor do sucesso da operação.
# SSD de Formato Breve #
![SSD UC 21.jpg](https://bitbucket.org/repo/4aRMAx/images/2628428100-SSD%20UC%2021.jpg)
# Formato Completo #
### Ator Principal ###
Gestor de Exposições
### Partes Interessadas e seus interesses ###
Gestor de Exposições-Pretende aceder a informação de Exposições anteriores.
### Pré Condições ###
### Pós Condições ###
Dados da Exposição serem carregados com sucesso. 
### Cenário de Sucesso ###
1. O gestor de exposições inicia o caso de uso de carregar dados de uma exposição.
2. O sistema solicita a escolha do ficheiro da exposição que o gestor pretende importar.
3. O gestor escolhe o ficheiro.
4. O sistema informa o organizador do sucesso da operação.
### Extensões ###
*a. A qualquer momento o Organizador solicita cancelamento do processo de decisão.
O caso de uso termina.

3a. O ficheiro escolhido não esta no formato correto 
   O sistema solicita a escolhe de um novo ficheiro(passo 2) 
##Requisitos especiais


##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
- 

##Questões em aberto .