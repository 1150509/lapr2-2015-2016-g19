# UC 2 - Definir FAE #

## Formato Breve ##

O organizador inicia o processo de definição do FAE . O sistema apresenta todas exposições para os quais é um dos organizadores. O organizador indica qual a exposição a considerar. O sistema solicita os dados de cada funcionário de apoio à organização. O organizador insere os dados. O sistema apresenta os dados e o organizador confirma os mesmos. O sistema regista a informação sobre os membros de apoio à exposição e informa o organizador que a operação foi realizada com sucesso.

## SSD Formato Breve ##

![SSD_UC2.png](https://bitbucket.org/repo/Lg4X4o/images/1032603971-SSD_UC2.png)

## Formato Completo (versão + simples e direta) ##

#### Ator principal ####
* Organizador
#### Partes interessadas e seus interesses ####
* Organizador: pretende que a informação dos FAE esteja correta e não tenha que ser reintroduzida.
* FAE:  pretende que a sua informação esteja correta. 
* Centro de Exposição: Pretende criar e manter registo dos FAE.

#### Pré-condições ####
* Utilizador autenticado no sistema como Organizador.
#### Pós-condições ####
* A informação dos membros do FAE criada é registada no sistema.
* Implica a classificação dos funcionários como “FAE”.

#### Cenário de sucesso principal (ou fluxo básico) ####
1. O organizador inicia no sistema o registo da FAE.
2. O sistema mostra a lista de exposições em que o utilizador é organizador.
3. O organizador seleciona a exposição pretendida.
4. O sistema solicita a identificação dum utilizador para membro da FAE.
5. O organizador introduz o identificador do novo membro.
6. O sistema valida e solícita confirmação.
7. O organizador confirma os dados.
8. O sistema regista o novo membro.
9. Os passos 4-8 são repetidos até a FAE estar completa.
10.O sistema atribui a FAE à exposição e informa o organizador que a operação foi realizada com sucesso.

#### Extensões (ou fluxos alternativos) ####
*a. A qualquer momento o organizador solicita o cancelamento da operação.
> O caso de uso termina.

2a. Não há exposições nas condições especificadas.
> O sistema avisa o organizador e o caso de uso termina.

5a. A identificação introduzida não corresponde a nenhum utilizador do sistema.
> 1. O sistema alerta para o facto.
> 2. O sistema permite a alteração da identificação introduzida (passo 5).
> >  2a. O organizador não altera os dados. O caso de uso termina.

5b. O sistema deteta que o membro indicado já está incluído na FAE.
> 1. O sistema alerta o organizador para o facto.
> 2. O sistema permite a sua alteração (passo 5).
> > 2a. O organizador não altera os dados. O caso de uso termina.

#### Requisitos especiais ####
* 
#### Lista de Variações de Tecnologias e Dados ####
* 
#### Frequência de Ocorrência ####
* 
#### Questões em aberto ####


* Alguma pessoa deve ser notificada da inserção de novos membros da FAE de uma exposição no sistema como, por exemplo, o seu diretor? Todos os organizadores devem receber também uma notificação?
* Qual a frequência de ocorrência deste caso de uso?