# UC 24 - Analisar qualidade de FAE #

## Formato Breve ##

O Organizador inicia a funcionalidade de analisar qualidade de FAE. O Sistema apresenta uma lista dos FAEs cuja variação entre suas avaliações e a Média geral das avaliações é maior que 1 e, cuja exposição o organizador que está a  utilizar a aplicação é de facto utilizador e dá a possibilidade de remover cada um desses FAES. O organizador seleciona os FAES que quer remover . O Sistema pede confirmação. O Organizador confirma. O Sistema remove os FAES selecionados e imprime uma mensagem de sucesso.

## SSD ##

![SSD - It4 - UC 24.jpg](https://bitbucket.org/repo/4aRMAx/images/1823017674-SSD%20-%20It4%20-%20UC%2024.jpg)

## Formato Completo ##

#### Ator principal ####

* Organizador

#### Partes interessadas e seus interesses ####

* Organizador

#### Pré-condições ####

* Utilizador atutenticado no sistema como organizador.

### Pós-condições ###

* Ter havido uma correta listagem de FAES.

### Cenário de sucesso principal (ou fluxo básico) ###
 
1. O Representante inicia a funcionalidade de Analisar qualidade de FAES.
2. O Sistema apresenta uma lista dos FAEs cuja variação entre suas avaliações e a Média geral das avaliações é maior que 1 e, cuja exposição o organizador que está a  utilizar a aplicação é de facto organizador e dá a possibilidade de remover cada um desses FAES.
3. O organizador seleciona os FAES que quer remover.
4. O Sistema pede confirmação.
5. O Organizador confirma a sua escolha.
6. O Sistema remove os FAES selecionados e imprime uma mensagem de sucesso.

#### Extensões (ou fluxos alternativos) ####

*a. O gestor de exposições solicita o cancelamento da operação.
> O caso de uso termina.

3a. O Organizador não seleciona nenhum FAE para remover
> 1. O sistema informa o Organizador desse facto e pede a confirmação da escolha da não remoção de FAES.
> 2a. O Organizador confirma.
> 3a. A aplicação termina.
> 2b. O Organizador não confirma.
> 3b. A aplicação regressa ao passo 2.

#### Requisitos especiais ####

* 

#### Lista de Variações de Tecnologias e Dados ####

* 

#### Frequência de Ocorrência ####

* 

#### Questões em aberto ####
*