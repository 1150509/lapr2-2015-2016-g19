** *UC5 – Criar Candidatura a Exposição*  **

## **Formato Breve**

O Representante do Expositor inicia a criação da candidatura à Exposição. O sistema a apresenta lista de exposições. O Representante seleciona exposição para a qual se candidata. O sistema solicita dados de candidatura. O Representante insere os dados solicitados (nome da empresa, morada, número de telemóvel, área pretendida e quantidade de convites). O Sistema valida os dados inseridos e solicita a introdução dos produtos a publicitar. O Representante insere os produtos que pretende publicitar. O Sistema confirma e solicita a introdução das palavras-chave acerca da candidatura que está a criar. O Representante insere na candidatura, no mínimo duas e no máximo cinco palavras chaves que caracterizem a candidatura. O Sistema pede confirmação. O representante confirma. O Sistema questiona se pretende a demonstração da exposição. O Representante responde. O Sistema pede confirmação. O Representante confirma. O sistema informa do sucesso da operação.

## **SSD de formato breve**
![SSD05.jpg](https://bitbucket.org/repo/4aRMAx/images/92455492-SSD05.jpg)
## **Formato Completo**

### *Ator principal*
Representante do Expositor.

### *Partes interessadas e seus interesses*
* Representante do Expositor: pretende registar uma nova candidatura.

### *Pré-condições*
* A candidatura não está registada;
* O representante encontra-se registado no sistema.

### *Pós-condições*
* A candidatura fica armazenada para avaliação.

### *Cenário de sucesso principal (ou fluxo básico)*
1.      O Representante do Expositor inicia a criação da candidatura à Exposição. 
2.      O Sistema a apresenta lista de exposições.  
3.      O Representante seleciona exposição para a qual se candidata.
4.      O Sistema solicita dados de candidatura. 
5.      O Representante insere os dados solicitados (nome da empresa, morada, número de telemóvel, área pretendida e quantidade de convites).
6.      O Sistema valida os dados inseridos e solicita a introdução dos produtos a publicitar.
7.      O Representante insere os produtos que pretende publicitar.
8.      O Sistema confirma e solicita a introdução das palavras-chave acerca da candidatura que está a criar.
9.      O Representante insere na candidatura, no mínimo duas e no máximo cinco palavras chaves que caracterizem a candidatura.
10.     O Sistema pede confirmação. 
11.     O Representante confirma. 
12.     O Sistema questiona se pretende a demonstração da exposição. 
13.     O Representante responde. 
14.     O Sistema pede confirmação 
15.     O Representante confirma. 
16.     O Sistema informa do sucesso da operação.

### *Extensões (ou fluxos alternativos)*

*a. O representante solicita cancelamento do registo da candidatura.

+ O caso de uso termina.

5a. Dados mínimos obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta.

    2.a O gestor de exposições não altera os dados. O caso de uso termina.


5b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1. O sistema alerta o gestor para o facto.
2. O sistema permite a sua alteração.

    2a. O gestor de exposições não altera os dados. O caso de uso termina.

12a. O sistema deteta que não há demonstração criada.

1. O sistema salta os passos 12 a 15.

### *Requisitos especiais*
-

### *Lista de variações em tecnologias e dados*
-

### *Frequência de Ocorrência*
-

### *Questões em aberto*
* Há mais dados para além dos enumerados?
* Quantos produtos pode um expositor ter?