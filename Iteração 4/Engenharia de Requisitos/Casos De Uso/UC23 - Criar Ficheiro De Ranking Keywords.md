## Formato Breve ##
O Gestor de Exposições inicia o caso de uso de criação do ficheiro Comma-Separated Value (CSV).O Sistema cria o ficheiro e informa do sucesso da operação.
# SSD Formato Breve #
![SSD NVO.jpg](https://bitbucket.org/repo/4aRMAx/images/1074841946-SSD%20NVO.jpg)
## Formato Completo	

####Ator Principal####
Gestor de Exposições

####Pré Condições####

####Pós Condições####
Ficheiro Criado com Sucesso

####Interessados e seus interesses####
Gestor de Exposições-Pretende que o ficheiro de estatística seja criado de forma a o conseguir consultar.

####Cenário de Sucesso:####
1-O gestor Inicia o caso de uso de criar o ficheiro de estatística Comma-Separated Value (CSV).
2-O sistema mostra uma lista de exposições e pede a seleção de uma.
3-O gestor seleciona uma exposição.
4-O sistema cria o ficheiro e informa o gestor do sucesso da operação.
#### Extensões (ou fluxos alternativos) ####
#### Requisitos especiais ####
* 
#### Lista de Variações de Tecnologias e Dados ####
* 
#### Frequência de Ocorrência ####
* 
#### Questões em aberto ####
 