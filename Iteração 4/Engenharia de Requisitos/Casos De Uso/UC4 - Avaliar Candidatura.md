# Formato breve #

O FAE inicia a funcionalidade de avaliação de candidatura. O sistema apresenta a lista de exposições do FAE em período de avaliação. O FAE seleciona uma exposição. O sistema apresenta a lista de candidaturas atribuídas ao FAE. FAE seleciona a candidatura a analisar. O sistema apresenta os critérios de avaliação da candidatura e solicita avaliação de cada campo de 0 a 5. O FAE introduz as respetivas avaliações e o texto justificativo. O sistema valida e pede confirmação. O FAE confirma os dados. O sistema regista os dados e informa do sucesso da operação.

## SSD de formato breve ##

![SSD4.jpg](https://bitbucket.org/repo/4aRMAx/images/2530471492-SSD4.jpg)

# Formato completo #

## Ator principal ##

Funcionário de apoio à exposição (FAE)

## Partes interessadas e seus interesses ##

Gestor de exposições: pretende que os FAE aceitem o apoio à sua exposição.

FAE: pretende aceitar ou recusar os pedidos de exposição que lhe foram atribuídos.

## Pré-condições ##

FAE é um utilizador registado no sistema.

Organizador de exposições ter atribuído pelo menos uma exposição ao utilizador.

## Pós-condições ##

A candidatura de exposição é aceite ou recusada pelo FAE, ao qual foi atribuída.

## Cenário de sucesso principal (ou fluxo básico) ##

1. O FAE inicia a funcionalidade de avaliação de candidatura.
2. O sistema apresenta a lista de exposições do FAE em período de avaliação.
3. O FAE seleciona uma exposição.
4. O sistema apresenta a lista de candidaturas atribuídas ao FAE.
5. O FAE seleciona a candidatura a analisar.
6. O sistema apresenta os dados da candidatura e os critérios de avaliação (conhecimento do FAE sobre o tema da exposição, adequação da candidatura à exposição, adequação da candidatura às demonstrações, adequação do número de convites por candidatura, recomendação global) e solicita avaliação de cada campo de 0 a 5.
7. O FAE introduz as suas avaliações.
8. O sistema valida os dados e solicita introdução de um texto justificativo.
9. O FAE introduz o texto justificativo.
10. O sistema valida os dados e solicita confirmação. 
11. O FAE confirma os dados.
12. O sistema regista os dados e reporta o sucesso da operação.

## Extensões (ou fluxos alternativos) ##

*a. O FAE solicita cancelamento da operação.

* O caso de uso termina.

8a. FAE não avalia todos os campos.

1. O sistema alerta o FAE para os dados em falta.

2. O sistema permite a sua alteração.

* O FAE não altera os dados. O caso de uso termina.

10a. FAE não introduz um texto justificativo.

1. O sistema informa para os dados em falta.

2. O sistema permite a introdução dos dados em falta.

* O FAE não altera os dados. O caso de uso termina.

## Requisitos especiais ##
-

## Lista de variações em tecnologias e dados ##
-

## Frequência de ocorrência ##

Enquanto existirem candidaturas por avaliar atribuídas ao FAE.

## Questões em aberto ##

Neste novo método de avaliação quantitativa, o FAE ainda precisa de introduzir um texto justificativo?
