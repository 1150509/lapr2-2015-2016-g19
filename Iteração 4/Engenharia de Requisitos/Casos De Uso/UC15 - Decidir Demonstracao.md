# UC15 Decidir Demonstração
##	Formato breve
O organizador inicia o processo de decidir demonstração. O sistema mostra as exposições que estão preparadas para iniciar este processo. O organizador seleciona uma exposição. O sistema mostra as demonstrações previstas para a exposição. O organizador seleciona quais as demonstrações que funcionarão. O sistema valida e pede que defina o período de candidaturas às demonstrações. O organizador insere as datas pretendidas. O sistema mostra os dados e pede confirmação. O organizador confirma. O sistema guarda a informação.

##	SSD de formato breve

![SSD15.jpg](https://bitbucket.org/repo/4aRMAx/images/3301924273-SSD15.jpg)

##	Formato completo

###Ator principal
+ Organizador

###Partes interessadas e seus interesses
+ Exposição: pretende saber que demonstrações vão funcionar.
+ Representantes: pretendem saber que demonstrações se poderão candidatar

###Pré-condições
+ Candidaturas aceites
+ Stands criados e atribuidos

###Pós-condições
+ Demonstrações que irão funcionar decididas

###Cenário de sucesso principal (ou fluxo básico)
1. O organizador inicia o processo de decidir demonstração. 
2. O sistema mostra as exposições que estão preparadas para iniciar este processo. 
3. O organizador seleciona uma exposição. 
4. O sistema mostra as demonstrações previstas para a exposição. 
5. O organizador seleciona quais as demonstrações que funcionarão. 
6. Os passos 4 e 5 repetem-se até que o organizador não queira selecionar mais demonstrações.
7. O sistema valida e pede que defina o período de candidaturas às demonstrações. 
8. O organizador insere as datas pretendidas. 
9. O sistema mostra os dados e pede confirmação. 
10. O organizador confirma.
11. O sistema guarda a informação.

###Extensões (ou fluxos alternativos)
*a. A qualquer momento o Organizador solicita cancelamento do processo de decisão.

    O caso de uso termina.

2a. Não há exposições nas condições mencionadas.

    O sistema informa o Organizador e o caso de uso termina.

4a. Não há demonstrações nas condições mencionadas.

    O sistema informa o Organizador e o caso de uso termina.

6a. O organizador não seleciona nenhuma demonstração.

    O sistema informa o Organizador que não selecionou nenhuma demonstração.
    O sistema permite a sua alteração (passo 5).


9a. O organizador não definiu nenhuma data para o período de candidaturas às demonstrações.

    O sistema informa o Organizador que não inseriu nenhuma data.
    O sistema permite a sua alteração (passo 8).

##Requisitos especiais


##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
- 

##Questões em aberto
+ Existe algum limite de demonstrações numa exposição?