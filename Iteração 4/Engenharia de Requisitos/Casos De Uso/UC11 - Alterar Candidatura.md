# UC11 - Alterar Perfil Candidatura #

O representante do expositor inicia a alteração de dados de uma candidatura. O sistema solicita a exposição a qual a candidatura se refere. O representante do expositor indica qual a exposição.O sistema qual a cancidatura.O Representante escolhe a candidatura. O sistema solicita os novos dados da candidatura (nome comercial da empresa, morada, telemóvel, área de exposição pretendida, os produtos a expor e a quantidade de convites a adquirir). O representante do expositor introduz os dados solicitados. O sistema valida e apresenta os dados ao representante do expositor, pedindo que os confirme. O representante do expositor confirma os dados da candidatura.O sistema pergunta ao  representante de exposições se a empresa está interessada em participar numa demonstração.O representante do expositor indica que sim.O sistema apresenta uma lista de demonstrações previamente criadas e solicita a seleção de uma ou mais demonstrações. O representante de exposições indica quais as novas demonstrações em que quer participar.O representante de exposições seleciona uma ou mais demonstrações.O sistema solicita a confirmação da seleção de demonstrações. O representante de exposições confirma.  O sistema altera os dados da candidatura e informa o sucesso.

## SSD - Formato Breve ##
![UC11 - SSD.jpg](https://bitbucket.org/repo/BKrx4b/images/179226424-UC11%20-%20SSD.jpg)

## Ator primário ##
* Representante do expositor

## Partes interessadas e seus interesses ##
* Centro de Exposição: pretende que os dados da Candidatura estejam actualizados.

* Representante do expositor: pretende que o processo de candidatura permita re-utilizar automaticamente informação já inserida em candidaturas anteriores (sobretudo dados relativos à identificação da empresa).

## Pré-condições ##
* Candidaturas criadas.

## Pós-condições ##
*Os dados da Candidatura sejam actualizados com sucesso.
 
## Cenário de sucesso principal (ou fluxo básico) ##
1. O representante do expositor inicia a alteração de uma Candidatura. 
2. O sistema solicita a exposição à qual a candidatura se refere.
3. O representante do expositor indica qual a exposição à qual quer alterar a candidatura.
4. O sistema solicita a candidatura à qual quer alterar os daos.
5. O Representante escolhe a candidatura.
6. O sistema solicita os novos dados da candidatura (nome comercial da empresa, morada, telemóvel, área de exposição pretendida e a quantidade de convites a adquirir).
7. O representante do expositor introduz os dados solicitados.
8. O sistema solicita os dados de um produto a expor (designação do produto).
9. O representante do expositor insere os dados solicitados.
10. Os passos 6 a 7 repetem-se até que todos os produtos a expor tenham sido inseridos.
11. O sistema valida e apresenta os novos  dados da candidatura ao representante do expositor, pedindo que os confirme.
12. O representante do expositor confirma os dados da candidatura.
13. O sistema pergunta ao representante de empresas se  a empresa que ele representa está interessada em que sejam realizadas uma ou mais demonstrações.
14.O representante de empresas indica há interesse por parte da empresa.
15. O sistema apresenta uma lista de demonstrações e solicita ao gestor de empresas a seleção de uma ou mais demonstrações.
16. O representante de exposições seleciona uma ou mais demonstrações.
17. O sistema solicita ao representante de uma empresa que este confirme a seleção de demonstrações.
18. O representante de exposições confirma.
19. O sistema valida as demonstrações.
20. O sistema regista os novos dados da candidatura e informa o representante do expositor do sucesso da operação. 

## Extensões (ou fluxos alternativos) ##
* *a. O representante do expositor solicita o cancelamento da operação. 
> 1. O caso de uso termina.
* 9a. A exposição indicada pelo representante do expositor não é reconhecida.
> 1. O sistema informa que a exposição indicada não é reconhecida.
> 2. O sistema permite nova introdução da exposição pretendida (passo 3).
* 9b. Dados mínimos obrigatórios relativos à candidatura em falta.
> 1. O sistema informa quais os dados em falta.
> 2. O sistema permite a introdução dos dados em falta (passo 5).
* 9c. O sistema deteta que os dados (ou algum subconjunto dos dados) relativos à candidatura introduzidos devem ser únicos e que já existem no sistema.
> 1. O sistema alerta o representante do expositor para o facto.
> 2. O sistema permite a sua alteração (passo 5).
* 9d. O sistema deteta que os dados (ou algum subconjunto dos dados) relativos à candidatura introduzidos são inválidos.
> 1. O sistema alerta o representante do expositor para o facto.
> 2. O sistema permite a sua alteração (passo 5).
* 17a. O sistema deteta que o representante de exposições não introduziu uma ou mais demonstrações.
>1. O sistema alerta o representante para o facto de que este não introduziu demonstrações.
>2. O sistema permite a sua alteração (passo 14).
## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##

* Quais os dados que em conjunto permitem detetar a duplicação de candidaturas a uma exposição?
* Alguém deve ser notificado que uma nova candidatura a uma dada exposição foi alterada no sistema (e.g. os organizadores)?
* Qual o número mínimo de produtos que um expositor pode inserir?
* Há um número máximo de produtos que um expositor pode incluir na candidatura?
* Qual a frequência de ocorrência deste caso de uso?

