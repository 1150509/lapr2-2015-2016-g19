# UC 1 - Criar Exposição #

## Formato Breve ##

O gestor de exposições inicia a criação de uma nova exposição. O sistema solicita os dados necessários (título, texto descritivo, data início, data fim, local de realização e organizadores). O gestor de exposições introduz os dados solicitados. O sistema valida e apresenta os dados ao gestor de exposições, pedindo que os confirme. Após a confirmação, o sistema regista a nova exposição e informa o gestor de exposições do sucesso da operação. 


## SSD Formato Breve ##

![SSD_UC1.jpg](https://bitbucket.org/repo/Lg4X4o/images/2182386381-SSD_UC1.jpg)


## Formato Completo ##

#### Ator principal ####
* Gestor de exposições
#### Partes interessadas e seus interesses ####
* Gestor de exposições: pretende que a criação de exposições seja mais rápida e com menos erros.
* Organizador: pretende que a informação da exposição esteja correta e não tenha que ser reintroduzida.
* FAE: pretende que a informação da exposição esteja correta e não tenha que ser reintroduzida.
#### Pré-condições ####
* Utilizador autenticado no sistema como Gestor de Exposições.
#### Pós-condições ####
* A informação da exposição criada é registada no sistema.
#### Cenário de sucesso principal (ou fluxo básico) ####
1. O gestor de exposições inicia no sistema a criação de uma nova exposição. 
1. O sistema solicita os dados necessários (título, texto descritivo, data início, data fim e local de realização). 
1. O gestor de exposições introduz os dados solicitados. 
1. O sistema mostra a lista de utilizadores do sistema que podem ser indicados como organizadores da exposição.
1. O gestor de exposições seleciona um utilizador como organizador.
1. O sistema guarda o utilizador/organizador selecionado.
1. Os passos 4 a 6 repetem-se até todos os utilizadores/organizadores da exposição estarem definidos. 
1. O sistema valida e apresenta todos os dados ao gestor de exposições, pedindo que os confirme. 
1. O gestor de exposições confirma. 
1. O sistema regista a nova exposição e informa o gestor de exposições do sucesso da operação.
#### Extensões (ou fluxos alternativos) ####
*a. O gestor de exposições solicita o cancelamento da operação.
> O caso de uso termina.

4a. A lista de utilizadores a mostrar é vazia.
> 1. O sistema informa o Gestor de Exposições desse facto.
> 2. O caso de uso termina.

6a. O sistema deteta que o utilizador selecionado já tinha sido previamente indicado como organizador da exposição.
> 1. O sistema informa o Gestor de Exposições desse facto.
> 2. O sistema permite a seleção de outro utilizador (passo 4).

7a. O sistema deteta que o gestor seleccionou menos de 2 organizadores.
> 1. O sistema informa numero invalido de organizadores.
> 2. O sistema permite a introdução de mais organizadores(passo 4).
> > 2a. O gestor de exposições não introduz mais organizadores.O caso de uso termina.

8a. Dados mínimos obrigatórios em falta.
> 1. O sistema informa quais os dados em falta.
> 2. O sistema permite a introdução dos dados em falta (passo 3).
> > 2a. O gestor de exposições não altera os dados. O caso de uso termina.

8b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
> 1. O sistema alerta o gestor de exposições para o facto.
> 2. O sistema permite a sua alteração (passo 3).
> > 2a. O gestor de exposições não altera os dados. O caso de uso termina.

8c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.  
> 1. O sistema alerta o gestor de exposições para o facto.
> 2. O sistema permite a sua alteração (passo 3)
> > 2a. O gestor de exposições não altera os dados. O caso de uso termina.
#### Requisitos especiais ####
* 
#### Lista de Variações de Tecnologias e Dados ####
* 
#### Frequência de Ocorrência ####
* 
#### Questões em aberto ####
* Quais são os dados obrigatórios para a criação de uma exposição?
* Quais os dados que em conjunto permitem detetar a duplicação de exposições?
* Caso um dos organizadores ainda não esteja registado no sistema, o gestor de exposições pode registá-lo?
* Qual a frequência de ocorrência deste caso de uso?