#UC 14 - Atualizar Conflito#

##Formato Breve##

O FAE inicia a funcionalidade de atualizar conflito. O Sistema mostra uma lista de conflitos e pergunta ao FAE qual é o conflito que o FAE quer alterar. O FAE escolhe um conflito. O Sistema pede a alteração do conflito. O FAE altera o conflito. O Sistema valida a alteração e pede confirmação. O FAE confirma. O Sistema regista a alteração.

##SSD Formato Breve##
![UC14 - SSD.jpg](https://bitbucket.org/repo/BKrx4b/images/1113234367-UC14%20-%20SSD.jpg)
##Formato Completo##


**Ator principal**

FAE

**Partes interessadas e seus interesses**

Gestor de exposições: pretende conseguir automatizar a gestão dos conflitos.

**Pré-condições**

Utilizador autenticado no sistema como FAE.

**Pós-condições**

A informação de conflitos é atulizada e registada com sucesso no sistema.


**Cenário de sucesso principal (ou fluxo básico)**

1. O FAE inicia no sistema a atualização de um tipo de conflito.

2.O sistema Pergunta qual o tipo de conflito que pretende alterar.

4.O Fae escolhe o Tipo de conflito

5. O sistema solicita os dados necessários.

6. O FAE introduz os dados solicitados.

7. O Sistema valida a informação introduzida e solicita confirmação.

8. O FAE confirma.

**Extensões**

*a. O gestor de exposições solicita o cancelamento da operação.

O caso de uso termina.

4a. A informação introduzida pelo utilizador não é válida.

1.O sistema informa o FAE desse facto e solicita a reintrodução da informação(passo 2).

Requisitos especiais
-
Lista de Variações de Tecnologias e Dados
-
Frequência de Ocorrência
-
Questões em aberto
-