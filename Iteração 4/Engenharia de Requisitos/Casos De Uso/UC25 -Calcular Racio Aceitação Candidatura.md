# UC25 Calcular Racio Aceitação Candidatura
##	Formato breve
O Gestor de Exposições ou o Organizador inicia o processo de Calcular Racio Aceitação Candidatura. O sistema mostra as exposições que estão prontas para executar este processo. O gestor de Exposições ou o organizador seleciona uma exposição. O sistema mostra as candidaturas dessa exposição. O gestor de Exposições ou o organizador seleciona uma candidatura. O sistema calcula o racio de aceitação da candidatura e mostra ao utilizador.

##	SSD de formato breve

![SSD25.jpg](https://bitbucket.org/repo/4aRMAx/images/2007957760-SSD25.jpg)

##	Formato completo

###Ator principal
+ Gestor de Exposições 
ou
+ Organizador

###Partes interessadas e seus interesses
+ Exposição: pretende saber qual o racio de aceitação de cada candidatura.
+ Gestor de Exposições e Organizador: pretende saber qual o racio de aceitação da candidatura.

###Pré-condições
+ Candidatura avaliada.

###Pós-condições
+ Racio de aceitação calculado.

###Cenário de sucesso principal (ou fluxo básico)
1. O Gestor de Exposições ou o Organizador inicia o processo de Calcular Racio Aceitação Candidatura. 
2. O sistema mostra as exposições que estão prontas para executar este processo. 
3. O gestor de Exposições ou o organizador seleciona uma exposição. 
4. O sistema mostra as candidaturas dessa exposição. 
5. O gestor de Exposições ou o organizador seleciona uma candidatura. 
6. O sistema calcula o racio de aceitação da candidatura e mostra ao utilizador.
7. Os passos 4 a 6 repetem-se até que o Gestor de Exposições ou o Organizador não queira selecionar mais candidaturas.

###Extensões (ou fluxos alternativos)
*a. A qualquer momento o Gestor de Exposições ou o Organizador solicita cancelamento do processo de decisão.

    O caso de uso termina.

2a. Não há exposições nas condições mencionadas.

    O sistema informa o Gestor de Exposições ou o Organizador e o caso de uso termina.

4a. Não há candidaturas nas condições mencionadas.

    O sistema informa o Gestor de Exposições ou o Organizador e o caso de uso termina.

##Requisitos especiais


##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
- 

##Questões em aberto
+ 