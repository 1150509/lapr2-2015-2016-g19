# UC7 Confirmar Registo de utilizador
##	Formato breve
Um Gestor de exposições inicia a confirmação do registo de utilizadores no sistema. 
O sistema apresenta o conjunto de utilizadores que efetuaram o registo e que ainda não foram confirmados no sistema. 
O Gestor de exposições seleciona um utilizador pretendido e submete-o ao sistema. 
O sistema valida os dados e pede confirmação.
O Gestor de exposições confirma os dados inseridos. 
O sistema regista a confirmação e informa o Gestor de exposições do sucesso da operação.

##	SSD de formato breve
![SSD_UC7.png](https://bitbucket.org/repo/Lg4X4o/images/1231974403-SSD_UC7.png)

##	Formato completo

###Ator principal
* Gestor de exposições

###Partes interessadas e seus interesses
+ Gestor de exposições: Pretende confirmar o registo de utilizadores para estes poderem aceder a funcionalidades específicas do sistema
+ Centro de Exposições: Pretende criar e manter registo dos utilizadores de determinadas funcionalidades

###Pré-condições
+ Utilizador autenticado no sistema como Gestor de Exposições.

###Pós-condições
* Os utilizadores selecionados ficam armazenados no sistema como utilizadores confirmados.

###Cenário de sucesso principal (ou fluxo básico)
1.	O Gestor de exposições inicia no sistema a confirmação de utilizadores registados.
2.	O sistema fornece ao Gestor de exposições informação acerca dos utilizadores registados e que ainda não foram confirmados.
3.	O Gestor de exposições introduz os dados de um utilizador a confirmar.
4.	O sistema valida os dados e solicita que o Gestor de exposições confirme a sua escolha.
5.	O Gestor de exposições confirma a sua escolha.
6.	O sistema regista a confirmação do utilizador.
7.  O sistema informa o Gestor de exposições do sucesso da operação.

###Extensões (ou fluxos alternativos)
*a. O Gestor de exposições solicita cancelamento da confirmação. 

+ O caso de uso termina.

6a. O sistema detecta que os dados (ou algum subconjunto dos dados) introduzidos são inválidos (não correspondem aos que já existem no sistema).

1. O sistema alerta o Gestor de exposição para o facto.
2. O sistema permite a sua alteração (passo 3)

	2a. O Gestor de exposição não altera os dados. O caso de uso termina.

##Requisitos especiais
* 

##Lista de variações em tecnologias e dados
* 

##Frequência de Ocorrência
* 

##Questões em aberto
+ A confirmação de registo de utilizadores deve estar sempre disponível, mesmo que não haja exposições registadas no sistema?
+ Devem os utilizadores ser notificados por email quando o seu perfil é confirmado no sistema pelo Gestor de exposição?
+ Qual a frequência de ocorrência deste caso de uso?