**UC23 – Decidir Candidatura à Demonstração**

## **Formato Breve**

 O Organizador inicia avaliação de candidatura. O Sistema apresenta uma lista de exposições. O Organizador seleciona uma exposição. O Sistema apresenta lista de demonstrações dessa exposição. O Organizador seleciona uma demonstração. O Sistema mostra a candidatura referente a essa demonstração e pede uma decisão à mesma. O Organizador aceita ou rejeita a candidatura. O Sistema pede confirmação. O Organizador confirma. O Sistema anuncia o sucesso da operação.

## **SSD de formato breve**
![UC23.jpg](https://bitbucket.org/repo/4aRMAx/images/451051375-UC23.jpg)

## **Formato Completo**

### *Ator principal*
Organizador 

### *Partes interessadas e seus interesses*
* Organizador: aceita ou rejeita as candidaturas;
* Organizador da Exposição: fica a saber se a sua candidatura foi ou não aceite.

### *Pré-condições*
* O Organizador está registado no sistema;
* Existem candidaturas a serem avaliadas.

### *Pós-condições*
* Resposta à candidatura por parte do Organizador.

### *Cenário de sucesso principal (ou fluxo básico)*
1.      O Organizador inicia o processo de decidir candidatura. 
2.      O Sistema apresenta uma lista de exposições.
3.      O Organizador seleciona uma exposição. 
4.      O Sistema apresenta uma lista de demonstrações dessa exposição.
5.      O Organizador seleciona uma demonstração.
6.      O Sistema mostra a candidatura referente a essa demonstração e pede uma decisão à mesma.
7.      O Organizador aceita ou rejeita a candidatura.
8.      O Sistema pede confirmação. 
9.      O Organizador confirma.
10.     O processo de avaliação de candidaturas repete-se (passos 4 a 9).
11.     O Sistema anuncia o sucesso da operação.


### *Extensões (ou fluxos alternativos)*

*a. O Organizador solicita cancelamento do processo de decisão.

+ O caso de uso termina.

1a. O Organizador não se encontra registado no sistema.

1. O sistema informa do erro.
2. O sistema permite corrigir o erro, encaminhando o Organizador para o registo de um novo perfil.

2a. O sistema deteta que não há demonstrações para que o Organizador possa decidir as respetivas candidatura.

1. O caso de uso termina.

4a. O sistema deteta que todas as candidaturas foram decididas.

1. O caso de uso termina.

### *Requisitos especiais*
-

### *Lista de variações em tecnologias e dados*
-

### *Frequência de Ocorrência*
-

### *Questões em aberto*
* O Organizador terá que escrever um breve texto justificativo aquando da decisão de uma candidatura?
* Há qualquer tipo de notificação?