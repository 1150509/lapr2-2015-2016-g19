# Formato Breve #

O utilizador inicia o caso de uso de actualizar perfil utilizador.O sistema pede ao utilizador que introduza os novos dados.O utilizador introduz os novos dados.O sistema pede ao utilizador que confirme os dados.O utilizador confirma.O sistema regista os dados e informa o utilizador do sucesso da operação.

## SSD de Formato Breve ##
![UC11 - SSD.jpg](https://bitbucket.org/repo/BKrx4b/images/1989149228-UC11%20-%20SSD.jpg)
## Formato Completo ##
##Ator principal
* Utilizador Registado

###Partes interessadas e seus interesses
+ Utilizador Registado: Pretende alterar os seus dados.
+ Centro de Exposições: Pretende ter os dados dos seus utilizadores  atualizados.

###Pré-condições
+ Utilizador estar autenticado.

###Pós-condições
* Os dados do utilizador estarem atualizados.

###Cenário de sucesso principal (ou fluxo básico)
1. O utilizador inicia no sistema a alteração do seu perfil.
2. O sistema solicita os novos dados do utilizador (credenciais de acesso, nome, e-mail).
3. O utilizador  introduz os dados solicitados.
4. O sistema valida e solicita que o utilizador confirme os dados inseridos.
5. O utilizador  confirma os dados.
6. O sistema regista os dados e informa o utilizador  do sucesso da operação.

###Extensões (ou fluxos alternativos)
*a. O utilizador  solicita cancelamento do registo.

+ O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta (passo 3)

    2.a O utilizador não altera os dados. O caso de uso termina.


4b. O sistema detecta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

1. O sistema alerta o utilizador para o facto.
2. O sistema permite a sua alteração (passo 3)

    2a. O utilizador não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1. O sistema alerta o utilizador para o facto.
2. O sistema permite a sua alteração (passo 3)
    
    2a. O utilizador não altera os dados. O caso de uso termina.

##Requisitos especiais
* 

##Lista de variações em tecnologias e dados
* 

##Frequência de Ocorrência
* 

##Questões em aberto