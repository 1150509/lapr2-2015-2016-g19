# Formato Breve #
O organizador inicia o caso de uso de criar demonstração. O sistema pede ao organizador para escolher uma exposição.

O organizador escolhe a exposição.O sistema pede a introdução dos dados (um código único (na exposição), uma 

descrição e a uma lista de recursos necessários).Sistema  valida os dados e pede confirmação. Organizador confirma os 

dos. O sistema guarda a demonstração e informa o Organizador o sucesso da operação.
# SSD Formato Breve #
![SSD8.jpg](https://bitbucket.org/repo/BKrx4b/images/1134650735-SSD8.jpg)
# Formato Completo #
Ator Primário:
 Organizador.

Partes interessadas e seus interesses: 
-Representante: precisa das demonstrações criadas para a poder escolher na candidatura.

Pré condições: O centro ter recursos definidos.

Pós condições: Demonstração criada com sucesso.

Cenário de Sucesso:

1-O organizador inicia o caso de uso para criar demonstração.

2- O sistema mostra uma lista de exposições e pede para escolher uma exposição.

3-O organizador escolhe a exposição.

4-O Sistema pede a introdução dos dados (um código único (na exposição), uma descrição e a uma lista de recursos 
necessários).

5-O organizador introduz os dados.

6- Sistema valida os dados e pede confirmação.

7- Organizador confirma os dados.

8- O sistema guarda a demonstração e informa o Organizador o sucesso da operação.

Extensões:
*a. O gestor de exposições solícita o cancelamento da operação.
               O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.

1-O sistema informa quais os dados em falta.

2a -O sistema permite a introdução dos dados em falta (passo 2).

2a -O gestor de exposições não altera os dados. O caso de uso termina.

6b- O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

1-O sistema alerta o organizador para o facto.

2-O sistema permite a sua alteração (passo 2).

2a- O gestor de exposições não altera os dados. O caso de uso termina.

6c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1-O sistema alerta o organizador para o facto.

2-O sistema permite a sua alteração (passo 2)

2a- O organizador não altera os dados. O caso de uso termina.

Requisitos especiais
•	

Lista de Variações de Tecnologias e Dados
•	

Frequência de Ocorrência
•	

Questões em aberto
O código da demostração é criado automaticamente ou o organizador envia o código.
