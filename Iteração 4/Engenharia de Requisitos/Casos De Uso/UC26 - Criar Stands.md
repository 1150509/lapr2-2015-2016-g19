# UC26 Criar Stands #

## Formato breve ##
O Gestor de Exposições inicia a criação de um stand. O Sistema solicita os dados acerca do stand. O Gestor de Exposições insere os dados. O Sistema valida os dados e pede confirmação. O Gestor confirma. O Sistema regista os dados e informa o sucesso da operação.


### SSD de formato breve ###

![SSD26.jpg](https://bitbucket.org/repo/4aRMAx/images/275038282-SSD26.jpg)

## Formato completo ##

### Ator principal ###

+ Gestor de Exposições.

### Partes interessadas e seus interesses ###

+ Gestor de Exposições: pretende criar um stand.
+ Centro de Exposições: pretende guardar os dados do stand.

### Pré-condições ###

+ Gestor de Exposições se encontre registado no Sistema.

### Pós-condições ###

+ O stand é criado.

### Cenário de sucesso principal (ou fluxo básico) ###

1. O Gestor de Exposições inicia a criação de um stand.
2. O Sistema solicita os dados acerca do stand.
3. O Gestor de Exposições insere os dados.
4. O Sistema valida os dados e pede confirmação.
5. O Gestor confirma.
6. O Sistema regista os dados e  informa o sucesso da operação.

### Extensões (ou fluxos alternativos) ###

*a. O representante solicita cancelamento do processo.

+ O caso de uso termina.

3a. O Gestor de Exposições insere os dados.

1. O sistema alerta para os dados em falta.

2. O sistema permite alteração dos dados.

+ O representante não altera. Caso de uso termina.



##Requisitos especiais

-

##Lista de variações em tecnologias e dados

-

##Frequência de Ocorrência



##Questões em aberto

+Quantos stands pode o Gestor de Exposições criar?