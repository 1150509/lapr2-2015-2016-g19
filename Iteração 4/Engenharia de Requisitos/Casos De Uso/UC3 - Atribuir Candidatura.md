# UC3 - Atribuir candidatura #
O organizador inicia o processo de atribuição de candidaturas. O sistema disponibiliza uma lista de exposições que já tem organizadores, FAE, demonstrações e data limite de submissão definidos e pede para que uma delas seja seleccionada. O organizador seleciona a exposição. O sistema valida e apresenta uma lista mecanismos de atribuição, pedindo que seleccione um mecanismo. O organizador escolhe o mecanismo. O sistema apresenta a atribuição e pergunta se aceita a atribuição.Se o organizador aceitar o sistema guarda a atribuição e informa o sucesso da operação,senão pede para escolher outro mecanismo e repete os paços.

## SSD - Formato Breve ##
![SSD 3.jpg](https://bitbucket.org/repo/BKrx4b/images/3420298895-SSD%203.jpg)
## Ator primário ##
* Organizador
## Partes interessadas e seus interesses ##
* Organizador: Pretende associar candidaturas dos expositores aos funcionários de apoio à exposição.
## Pré-condições ##
* Utilizador autenticado no sistema como Organizador.
## Pós-condições ##
* A tabela de associações de candidaturas a FAE é atualizada.
## Cenário de sucesso principal (ou fluxo básico) ##

* 1-  O organizador inicia no sistema a atribuição de candidaturas.
* 2- O sistema mostra uma lista de exposições ativas e solicita escolha de uma.
* 3- O organizador seleciona uma exposição.
* 4- O sistema pede confirmação.
* 5- O organizador confirma.
* 6-O sistema pergunta qual o mecanismo que o organizador pretende.
* 7-O organizador escolhe o mecanismo
* 8-Sistema mostra a atribuiçao e pergunta ao organizador se confirma a atribuição
* 9-O organizador confirma.
* 10-O sistema guarda a atribuiçao e envia mensagem de sucesso.



## Extensões (ou fluxos alternativos) ##
* *a. A qualquer momento o organizador cancela o processo. 
> O caso de uso termina.
* 2a. Não há exposições nas condições especificadas.
> O sistema avisa o organizador e o caso de uso termina.
* 3a. A exposição introduzida pelo organizador não está nas condições necessárias.
> O sistema alerta para o facto.
> O sistema permite a alteração da identificação introduzida.
* 4a. Não há candidaturas à exposição.
> O sistema avisa o organizador e o caso de uso termina.
*8a. O organizador não confirma a atribuição.
> O sistema pede a escolha de outro mecanismo.
> O organizador nao escolhe outro mecanismo.
> O caso de uso termina
## Requisitos especiais ##
* 
## Lista de variações em tecnologias e dados ##
* 
## Frequência de Ocorrência ##
* 
## Questões em aberto ##

* Os FAE devem ser notificados da atribuição da candidatura? Como?