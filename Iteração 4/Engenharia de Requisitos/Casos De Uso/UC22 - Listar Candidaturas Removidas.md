# Formato Breve #
O organizdor inicia o caso de uso de Listar candidaturas removidas.O sistema mostra a lista de exposições do organizador e solicita a escolha de uma.Organizador selecciona uma exposição.O sistema mostra a lista das candidaturas a exposição removidas.
# SSD Formato Breve #
![SSD UC22.jpg](https://bitbucket.org/repo/4aRMAx/images/3542665268-SSD%20UC22.jpg)
# Formato Completo #

###Ator Principal###
Organizador
### Pré Condições ###
### Pós Condições ###
Lista ser mostrada com sucesso.
### Cenário de Sucesso ###
1-Organizador inicia o caso de uso de listar candidaturas removidas.
2-O sistema mostra as exposições do organizador e solicita a escolha de uma.
3-O organizador seleciona uma exposição.
3-O sistema mostra a lista de candidaturas removidas.
### Extensões ###
*a. O organizador solicita o cancelamento da operação.
> O caso de uso termina.
#### Requisitos especiais ####
* 
#### Lista de Variações de Tecnologias e Dados ####
* 
#### Frequência de Ocorrência ####
* 
#### Questões em aberto ####