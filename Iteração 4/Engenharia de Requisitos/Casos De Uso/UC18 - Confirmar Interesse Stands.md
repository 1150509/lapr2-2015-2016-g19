# UC18 Confirmar Interesse Stands #

## Formato breve ##

O representante inicia o processo de confirmar interesse nos stands. O sistema apresenta uma lista das exposições com as candidaturas aceites. O representante seleciona uma exposição. O sistema mostra as candidaturas do representante com stands atribuídos para a exposição selecionada. O representante seleciona uma candidatura. O sistema apresenta o stand atribuído a essa candidatura e solicita a decisão do representante. O representante analisa a informação e aceita ou rejeita o stand. O sistema valida e pede a confirmação dos dados. O representante confirma os dados. O sistema guarda a informação e reporta o sucesso.

### SSD de formato breve ###

![SSD18.jpg](https://bitbucket.org/repo/4aRMAx/images/2306867761-SSD18.jpg)

## Formato completo ##

### Ator principal ###

+ Representante de expositor

### Partes interessadas e seus interesses ###

+ Expositor: pretende confirmar o stand da sua candidatura de exposição.
+ Centro de Exposições: pretende saber se os stands vão ser utilizados.

### Pré-condições ###

+ Candidatura registada na exposição.
+ Stand atribuído à candidatura.

### Pós-condições ###

+ Candidatura aceite ou recusada.

### Cenário de sucesso principal (ou fluxo básico) ###

1. O representante inicia o processo de confirmar interesse nos stands.
2. O sistema apresenta uma lista de exposições e solicita seleção de uma delas.
3. O representante seleciona uma exposição.
4. O sistema mostra as candidaturas do representante com stands atribuídos para a exposição selecionada.
5. O representante seleciona uma candidatura.
6. O sistema apresenta o stand atribuído a essa candidatura e solicita a decisão do representante.
7. O representante analisa a informação e aceita ou rejeita o stand.
8. O sistema valida e pede a confirmação dos dados.
9. O representante confirma os dados.
10. O sistema guarda a informação e reporta o sucesso.

### Extensões (ou fluxos alternativos) ###

*a. O representante solicita cancelamento do processo.

+ O caso de uso termina.

4a. Não existem candidaturas que satisfaçam os critérios de busca.

1. O sistema alerta para os dados em falta.

2. O sistema permite seleção de nova exposição.

+ O representante não altera. Caso de uso termina.

8a. O representante não introduz uma decisão e introduz uma decisão inválida.

1. O sistema alerta para os dados inválidos.

2. O sistema permite alteração dos dados.

+ O representante não altera. Caso de uso termina.


##Requisitos especiais

-

##Lista de variações em tecnologias e dados

-

##Frequência de Ocorrência

Enquanto o representante tiver stands atribuídos à sua exposição por confirmar. 

##Questões em aberto

A candidatura é removida se o Representante rejeitar o stand atribuído?
