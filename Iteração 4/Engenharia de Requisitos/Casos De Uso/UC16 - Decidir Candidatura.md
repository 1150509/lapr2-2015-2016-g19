# UC16 Decidir Candidatura
##	Formato breve
O organizador inicia o processo de decidir candidatura. O sistema mostra as exposições que estão preparadas para iniciar este processo. O organizador seleciona uma exposição . O sistema mostra as candidatura previstas para a exposição. O organizador seleciona uma candidatura. O sistema mostra as avaliações feitas pelos FAEs para a candidatura selecionada. O organizador analisa a informação e aceita ou rejeita a candidatura. O sistema pede a confirmação dos dados. O organizador confirma os dados. O sistema guarda a informação.

##	SSD de formato breve

![SSD16.jpg](https://bitbucket.org/repo/4aRMAx/images/1774567272-SSD16.jpg)

##	Formato completo

###Ator principal
+ Organizador

###Partes interessadas e seus interesses
+ Exposição: pretende saber que candidaturas foram aceites para a exposição.
+ Expositor: pretende saber se a sua candidatura foi aceite ou não

###Pré-condições
+ Exposição criada no sistema.
+ Candidaturas criadas para essa exposição.

###Pós-condições
+ Candidatura aceite ou recusada.

###Cenário de sucesso principal (ou fluxo básico)
1. O organizador inicia o processo de decidir candidatura. 
2. O sistema mostra as exposições que estão preparadas para iniciar este processo. 
3. O organizador seleciona uma exposição. 
4. O sistema mostra as candidatura previstas para a exposição. 
5. O organizador seleciona uma candidatura. 
6. O sistema mostra as avaliações feitas pelos FAEs para a candidatura selecionada. 
7. O organizador analisa a informação e aceita ou rejeita a candidatura. 
8. O sistema pede a confirmação dos dados. 
9. O organizador confirma os dados. 
10. O sistema guarda a informação.
11. Os passos 4 a 10 repetem-se até que o organizador não queira selecionar mais candidaturas.

###Extensões (ou fluxos alternativos)
*a. A qualquer momento o Organizador solicita cancelamento do processo de decisão.

    O caso de uso termina.

2a. Não há exposições nas condições mencionadas.

    O sistema informa o Organizador e o caso de uso termina.

4a. Não há candidaturas nas condições mencionadas.

    O sistema informa o Organizador e o caso de uso termina.

6a. Candidatura não está avaliada pelos FAEs.

    O sistema informa o Organizador que não selecionou nenhuma demonstração.
    O sistema permite a sua alteração (passo 5).

8a. O organizador não aceitou nem rejeitou a candidatura.

    O sistema informa o Organizador que não selecionou nenhuma opção.
    O sistema permite a sua alteração (passo 7).

##Requisitos especiais


##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
- 

##Questões em aberto
+ Só um organizador decide todas as candidaturas ou mais que um?