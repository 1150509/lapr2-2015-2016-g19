# UC17 Atribuir Stands #

## Formato breve ##

O organizador inicia o processo de atribuir stands. O sistema apresenta uma lista das exposições do organizador com candidaturas decididas. O organizador seleciona uma exposição. O sistema mostra as candidaturas aceites da exposição selecionada. O organizador seleciona uma candidatura. O sistema apresenta a lista dos stands disponíveis e solicita seleção de um para atribuição. O organizador seleciona um stand. O sistema valida e pede a confirmação dos dados. O representante confirma os dados. O sistema regista a atribuição e reporta o sucesso.

### SSD de formato breve ###

![SSD17.jpg](https://bitbucket.org/repo/4aRMAx/images/237596596-SSD17.jpg)

## Formato completo ##

### Ator principal ###

+ Organizador

### Partes interessadas e seus interesses ###

+ Organizador: pretende atribuir os stands disponíveis às candidaturas submetidas.
+ Representante do expositor: pretende ter um stand atribuído à sua candidatura.

### Pré-condições ###

+ Pelo menos uma candidatura registada na exposição.

### Pós-condições ###

+ Stands atribuídos à candidaturas.

### Cenário de sucesso principal (ou fluxo básico) ###

1. O organizador inicia o processo de atribuir stands.
2. O sistema apresenta uma lista de exposições do organizador com candidaturas decididas.
3. O organizador seleciona uma exposição.
4. O sistema mostra as candidaturas aceites da exposição selecionada.
5. O organizador seleciona uma candidatura.
6. O sistema apresenta a lista dos stands disponíveis e solicita seleção de um para atribuição.
7. O organizador seleciona um stand.
8. O sistema valida e pede a confirmação dos dados.
9. O organizador confirma os dados.
10. O sistema regista a atribuição e reporta o sucesso.

* Os passos 6 a 11 repetem-se enquanto existirem candidaturas a demonstração sem stands atribuídos e/ou até o organizador interromper o processo.

### Extensões (ou fluxos alternativos) ###

*a. O organizador solicita cancelamento do processo.

+ O caso de uso termina.

4a. Não existem candidaturas aceites na exposição selecionada.

1. O sistema alerta para os dados em falta.

2. O sistema permite escolha de nova exposição.

+ O organizador não altera. Caso de uso termina.

8a. O organizador não seleciona um stand ou seleciona um stand inválido.

1. O sistema alerta para os dados inválidos.

2. O sistema permite alteração dos dados.

+ O organizador não altera. Caso de uso termina.


## Requisitos especiais ##

-

## Lista de variações em tecnologias e dados ##

-

## Frequência de Ocorrência ##

Enquanto existirem candidaturas aceites sem stands atribuídos.

## Questões em aberto ##

O que acontece se existirem mais candidaturas aceites que stands disponíveis?
O organizador tem isto em conta quando aceita ou rejeita as candidaturas?
