## Formato Breve ##
O Gestor de Exposições inicia o caso de uso de criação do ficheiro Comma-Separated Value (CSV).O Sistema cria o ficheiro e informa do sucesso da operação.

## Formato Completo ##	

####Ator Principal####
Gestor de Exposições

####Pré Condições####

####Pós Condições####
Ficheiro Criado com Sucesso

####Interessados e seus interesses####
Gestor de Exposições-Pretende que o ficheiro de estatística seja criado de forma a o conseguir consultar.

####Cenário de Sucesso:####
1-O gestor Inicia o caso de uso de criar o ficheiro de estatística Comma-Separated Value (CSV).
2-O sistema cria o ficheiro e informa o gestor do sucesso da operação.
#### Extensões (ou fluxos alternativos) ####
#### Requisitos especiais ####
* 
#### Lista de Variações de Tecnologias e Dados ####
* 
#### Frequência de Ocorrência ####
* 
#### Questões em aberto ####
 