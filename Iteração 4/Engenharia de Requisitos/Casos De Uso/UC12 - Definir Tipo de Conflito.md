# UC 12- Definir Tipo Conflito #

## Formato Breve ##

O Gestor de exposições inicia a funcionalidade de definir um tipo de conflito. O Sistema solicita a introdução da informação necessária para a definição de um conflito. O Gestor de Exposição introduz a informação. O Sistema valida a informação introduzida e solicita confirmação. O Gestor de Exposição confirma. O Sistema regista a informação do conflito.

## SSD Formato Breve ##
![UC12 - SSD.jpg](https://bitbucket.org/repo/BKrx4b/images/3324447917-UC12%20-%20SSD.jpg)
## Formato Completo ##

**Ator principal**

Gestor de exposições

**Partes interessadas e seus interesses**

Gestor de exposições: pretende conseguir automatizar a gestão dos conflitos.

**Pré-condições**

Utilizador autenticado no sistema como Gestor de Exposições.

**Pós-condições**

A informação de conflitos é criada e registada com sucesso no sistema.


**Cenário de sucesso principal (ou fluxo básico)**

1. O gestor de exposições inicia no sistema a definição de um tipo de conflito.

2. O sistema solicita os dados necessários.

3. O gestor de exposições introduz os dados solicitados.

4. O Sistema valida a informação introduzida e solicita confirmação.

5. O gestor de exposições confirma.

6. O Sistema regista a informação do conflito, gera os mecanismos de conflito correspondentes e informa o gestor de exposições do sucesso da operação.

**Extensões**

*a. O gestor de exposições solicita o cancelamento da operação.
O caso de uso termina.

4a.

A informação introduzida pelo utilizador não é válida.

1.O sistema informa o Gestor de Exposições desse facto e solicita a reintrodução da informação(passo 2).

Requisitos especiais
-
Lista de Variações de Tecnologias e Dados
-
Frequência de Ocorrência
-
Questões em aberto
-