** *UC20 – Criar Candidatura a Demonstração*  **

## **Formato Breve**

O Representante do Expositor inicia a criação da candidatura à Demonstração. O Sistema apresenta uma lista de demonstrações das escolhidas pelo Representante do Expositor e posteriormente decididas pelo Organizador. O Representante do Expositor seleciona uma Demonstração. O sistema solicita os dados de candidatura. O Representante do Expositor insere os dados (nome da empresa, morada, número de telemóvel, área pretendida, número de convites e o produto). O sistema pede confirmação dos dados colocados. O Representante do Expositor confirma. O sistema regista os dados informa o sucesso da operação.

## **SSD de formato breve**
![Sequence Diagram1.jpg](https://bitbucket.org/repo/4aRMAx/images/372444380-Sequence%20Diagram1.jpg)
### *Ator principal*
Representante do Expositor.

### *Partes interessadas e seus interesses*
* Representante do Expositor - Pretende criar uma candidatura para a Demonstração.

### *Pré-condições*
* A candidatura à Demonstração não se encontra criada.
* O utilizador tem de estar registado no sistema.

### *Pós-condições*
* A candidatura fica armazenada para avaliação.

### *Cenário de sucesso principal (ou fluxo básico)*

1.      O Representante do Expositor inicia a criação da candidatura à Demonstração.
2.       O Sistema apresenta uma lista de demonstrações das escolhidas pelo Representante do Expositor e posteriormente decididas pelo Organizador.
3.      O Representante do Expositor seleciona uma Demonstração.
4.      O sistema solicita os dados de candidatura. 
5.      O Representante do Expositor insere os dados (nome da empresa, morada, número de telemóvel, área pretendida, número de convites e o produto).
6.      O sistema pede confirmação dos dados colocados.
7.      O Representante do Expositor confirma.
8.      O sistema regista os dados e informa o sucesso da operação.

### *Extensões (ou fluxos alternativos)*

*a. O representante solicita cancelamento do registo da candidatura.
O caso de uso termina.

5a. Dados mínimos obrigatórios em falta.
1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta.

5b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
1. O sistema alerta o gestor para o facto.
2. O sistema permite a sua alteração.

2a. Não há Demonstrações avaliadas pelo Organizador.
A candidatura não pode ser efetuada.


### *Requisitos especiais*
-

### *Lista de variações em tecnologias e dados*
-

### *Frequência de Ocorrência*
-

### *Questões em aberto*
* Os FAE devem ser notificados da criação da candidatura? De que forma?