#Especificação suplementar

##Funcionalidades
*Especifica as funcionalidades que não se relacionam com os casos de uso, nomeadamente: Auditoria, Reporte, Interoperabilidade e Segurança.*

**Segurança/Autenticação: a utilização do sistema pelo Gestor de exposições, organizadores, FAE e representante exige autenticação.**

##Usabilidade
*Avalia a interface com o utilizador. Possui diversas subcategorias, entre elas: prevenção de erros; estética e design; ajudas (Help) e documentação; consistência e padrões.*

**A interação entre os utilizadores e o sistema deve ser simples, intuitiva e completamente adaptada à ação em causa.**

##Fiabilidade/Confiabilidade
*Refere-se a integridade, conformidade e interoperabilidade do software. Os requisitos a serem considerados são: frequência e gravidade de falha, possibilidade de recuperação, possibilidade de previsão, exatidão, tempo médio entre falhas.*

##Desempenho
*Avalia os requisitos de desempenho do software, nomeadamente: tempo de resposta, consumo de memória, utilização da CPU, capacidade de carga e disponibilidade da aplicação.*

##Suportabilidade
*Os requisitos de suportabilidade agrupam várias características, como: testabilidade, adaptabilidade, manutibilidade, compatibilidade, configurabilidade, instalabilidade, escalabilidade entre outros.*

**Estes mecanismos são baseados em diferentes critérios (e.g. número de FAE pretendidos por candidatura, distribuição de carga equitativa pelos FAE, experiência profissional na realização de exposições).** 


###	Restrições de design
*Especifica ou restringe o processo de design do sistema. Exemplos podem incluir: padrões de design, processo de desenvolvimento de software, uso de ferramentas de desenvolvimento, biblioteca de classes, etc.*

**Adoção do processo de desenvolvimento de software iterativo e incremental.**

**Adoção de boas práticas de design, nomeadamente padrões GRASP.**

###Restrições de implementação
*Especifica ou restringe o código ou a construção de um sistema tais como: normas de implementação, linguagens de implementação, políticas de integridade de base de dados, limites de recursos, sistema operativo.*

** O núcleo principal do software deve ser desenvolvido em Java.**

** Adoção de normas de codificação, nomeadamente Camel case.**

** Adoção de controle de versões.**


###Restrições de interface
*Especifica ou restringe as funcionalidades inerentes a interface entre o sistema e outros.*

###Restrições físicas
*Especifica uma limitação ou requisito físico do hardware utilizado, por exemplo: material, forma, tamanho ou peso.*