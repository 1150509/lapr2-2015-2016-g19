Avaliação: Processo de análise e selecção sobre a aceitação ou rejeição de candidaturas a uma exposição, conduzido por um ou mais FAE.

Candidatura: Pedido elaborado pelo representante do expositor para expor os seus produtos numa exposição.

Carga: conjunto de tarefas a ser executadas em simultâneo/conjunto.

Centro de exposições: Entidade que adquiriu a aplicação. AKA dono do software.

Conflito: Informação que não está de acordo com algum padrão definido ou que já existe.

Credenciais de Acesso: Informação que permite autenticar univocamente um utilizador perante o sistema. Numa das suas formas mais simples corresponde a um par username/password.

Demonstração: exibição de uma breve apresentação, por parte do representante, do conteúdo da exposição.

Demonstração da Exposição: Definida pelos organizadores da exposição e realizada no decorrer de uma exposição, esta demonstração baseia-se num código único, numa descrição e numa lista de recursos necessários disponíveis no centro de exposições.

Distribuição (de candidaturas): Processo através do qual um dos organizadores atribui a responsabilidade de seleção de cada uma das candidaturas dos expositores submetidos à exposição a uma ou mais pessoas da equipa de apoio à exposição.

Exposição: Apresentação/manifestação pública ou privada de materiais/produtos, que tem objetivos específicos e se desenvolve num determinado espaço durante um período de tempo.

Expositor: Empresa que pretende/irá expor produtos numa exposição.

FAE: Abreviatura de “Funcionários de apoio à exposição”.

Gestor de exposições: Pessoa responsável pela criação de exposições.

Lista de demonstrações: inventário que possui o tipo de demonstrações a ocorrer na exposição.

Local de realização: TBD.

Mecanismo de Distribuição: conjunto de critérios que permitem a seleção de fatores de atribuição de um FAE a uma candidatura.

Nome comercial da empresa: Organização à qual um expositor está associado através de uma relação de trabalho.
Organizador: Pessoa que é responsável pela organização de uma exposição.

Perfil de utilizador: Informação relativa a um utilizador do sistema.

Período (da exposição): Corresponde ao intervalo de tempo compreendido entre as datas de início e de fim da exposição.

Produto: Informação relativa aos produtos que o expositor pretende expor na exposição.

Registo de utilizador: Processo através do qual uma pessoa solicita o acesso ao sistema por forma a operar o sistema.

Recurso: ferramenta necessária para a realização de uma demonstração.

Representante (do expositor): Pessoa que representa a empresa/expositor e elabora a candidatura
Software: sequência de instruções escritas para serem interpretadas por um computador com o objetivo de executar tarefas específicas.

Stand: Espaço destinado à exposição dos produtos de uma empresa.

Utilizador: Representa uma pessoa que utiliza o sistema.

Utilizador não registado: Representa uma pessoa que utiliza o sistema anonimamente e se pode registar.