# Visão #

A Visão inclui as grandes ideias sobre:

 -Porque é que o projecto foi proposto

 -Quais são os problemas a resolver

 -Quem são as partes interessadas (“stakeholders”)

 -Quais são as suas necessidades

 -Perspectiva da solução proposta


## Introdução: ##

Perspectivamos uma aplicação ???

## Posicionamento: ##



## Oportunidade de negócio (o que é que os sistemas POS existentes não conseguem fazer) ##

 “Problem Statement” (problemas causados por falta de características): 

 ?

 “Product Position Statement” (para quem é o sistema, características marcantes, em que é diferente dos  competidores):

 ?


## Descrição das partes interessadas (distinguir entre Utilizador e Não Utilizador) ##
 Sumário: 

 ?

 Objectivos:

 ?


## Descrição genérica do Produto ##

 Perspectiva do Produto: 

 ?

 Benefícios do produto: 

 ?

 Custo e Preço:

 ?

 Licenciamento e instalação:

 ?

## Sumário das características do sistema: ##
?

## Outros requisitos e restrições: ##
?