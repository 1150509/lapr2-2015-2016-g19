# Realização do UC21 Carregar Dados Exposicao Anterior #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “CarregaDadosExposicaoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------

1.O gestor inicia a funcionalidade de carregar dados de exposições anteriores.|na||
2.O sistema solicita a escolha do ficheiro com a informação da exposição que o gestor pretende importar|...mostra os ficheiros|FicheirosExposicao|Regra Creator 4
3.O gestor seleciona um ficheiro.|||
4.O sistema guarda a exposição e informa o gestor do sucesso da operação.|...guarda a exposição|RegistoExposicoes|RC1

# Diagrama de Sequencia #
![DS UC 21.jpg](https://bitbucket.org/repo/4aRMAx/images/3783310409-DS%20UC%2021.jpg)

# Diagrama de Classes #

![dc21.jpg](https://bitbucket.org/repo/4aRMAx/images/3707829320-dc21.jpg)