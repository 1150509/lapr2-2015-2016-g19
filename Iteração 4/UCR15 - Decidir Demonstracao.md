# Diagrama de Responsabilidades #

![DiagramaResponsabilidade15.png](https://bitbucket.org/repo/4aRMAx/images/4168191367-DiagramaResponsabilidade15.png)

# Diagrama de Sequência #

![ds15.jpg](https://bitbucket.org/repo/4aRMAx/images/181598262-ds15.jpg)
![ds15_1.jpg](https://bitbucket.org/repo/4aRMAx/images/3134869076-ds15_1.jpg)
![ds15_2.jpg](https://bitbucket.org/repo/4aRMAx/images/1352092585-ds15_2.jpg)

# Diagrama de Classes #

![dc15.jpg](https://bitbucket.org/repo/4aRMAx/images/1730240622-dc15.jpg)