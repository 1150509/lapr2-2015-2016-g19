# Tabela de Responsabilidades UC24 - Analisar Qualidade FAE #


1. O organizador inicia no sistema a Análise da qualidade dos FAES. | n/a | | 
2. O sistema apresenta uma lista de exposições e solicita a escolha de uma delas. | Quem tem os Dados das Exposições | RegistoExposições | Regra Creator | IE: no DC RegistoExposições tem várias Exposições
3. O Organizador seleciona uma exposição | Quem guarda a seleção? | AnalisarQualidadeFAEController| O Controller é responsável pela interação entre o utilizador e o sistema.
5. O Sistema calcula os FAES cujas variações em relação à media geral das avaliações dos FAES é superior a 1.  | Quem tem os dados necesários ao calculo? |Avaliação | IE: A avaliação tem as avaliações e os FAEs que as deram.
Quem é responsável pelo cálculo?| Calculo| Classe necessária para aumentar diminuir o acopulamento e para realizar as operações matemáticas.
6. O Sistema apresenta a lista de FAES cujas variações em relação à media geral das avaliações dos FAES é superior a 1 e solicita a seleção de um deles para remoção|n/a | 
7. O organizador seleciona uma candidatura para remoção | Quem guarda a seleção? | AnalisarCandidaturaController| O Controller é responsável pela interação entre o utilizador e o sistema. 
8. O Sistema remove a candidatura selecionada. | Quem guarda a candidatura removida? | RegistoCandidaturas | Esta classe contém objetos da classe Candidaturas.

# DS UC 24 Analisar qualidade de FAE #

![UC 24 - Analisar qualidade de FAE .jpg](https://bitbucket.org/repo/4aRMAx/images/1319225656-UC%2024%20-%20Analisar%20qualidade%20de%20FAE%20.jpg)

# CalcularFaesVariacaoExcessiva #

![CalcularFaesVariacaoExcessiva_UC24.jpg](https://bitbucket.org/repo/4aRMAx/images/1507280552-CalcularFaesVariacaoExcessiva_UC24.jpg)

# Get Lista Atribuicoes Avaliadas Interaction use #

![getListaAtribuicoesAvaliadas.jpg](https://bitbucket.org/repo/4aRMAx/images/2518005174-getListaAtribuicoesAvaliadas.jpg)

# DC UC 24 Analisar qualidade de FAE #

![DC UC 24 It 4.jpg](https://bitbucket.org/repo/4aRMAx/images/551876180-DC%20UC%2024%20It%204.jpg)

