# Tabela de Responsabilidades #

![17.JPG](https://bitbucket.org/repo/4aRMAx/images/3518323845-17.JPG)

# Diagrama de Sequência #

![DS17.jpg](https://bitbucket.org/repo/4aRMAx/images/4265429002-DS17.jpg)

## Diagrama de Exposições de Organizador com Candidaturas decididas ##

![ExposicoesOrganizador.jpg](https://bitbucket.org/repo/4aRMAx/images/1569897832-ExposicoesOrganizador.jpg)

## Diagrama de Candidaturas Aceites ##

![CandidaturasAceites.jpg](https://bitbucket.org/repo/4aRMAx/images/858470434-CandidaturasAceites.jpg)

## Diagrama de tornar Stand indisponível ##

![Stand Indisponivel.jpg](https://bitbucket.org/repo/4aRMAx/images/4100618457-Stand%20Indisponivel.jpg)

## Diagrama de alteração de Estado de Candidatura ##

![StandAtribuidoState.jpg](https://bitbucket.org/repo/4aRMAx/images/424040940-StandAtribuidoState.jpg)

## Diagrama de Classes ##

![DC17.jpg](https://bitbucket.org/repo/4aRMAx/images/2830226040-DC17.jpg)
