## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “RegistarUtilizadorController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

1.	O ator inicia no sistema o seu registo.
    1.	Questão: Que classe interage com o ator?
        +	Resposta: A classe RegistarUtilizadorUI.
        +	Justificação: Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio.
    1.	Questão: Que classe coordena o UC?
        +	Resposta: RegistarUtilizada classe orController.
        +	Justificação: Controller.
    1.	Questão: Que classe instancia Utilizador?
        +	Resposta: Registoutilizador
        +	Justificação: Creator (Regra 1)
2.	O sistema solicita os dados do utilizador (credenciais de acesso, nome, e-mail, instituição de afiliação).
    2.	Questão: Que classe solicita os dados?
        +	Resposta: RegistarUtilizadorUI.
        +	cf. 1.1
3.	O utilizador  introduz os dados solicitados.
    3.	Questão: Onde ser guardam os dados do utilizador?
        +	Resposta: Utilizador
        +	Justificação: Information Expert
4.	O sistema valida e regista os dados do utilizador.
    4.	Questão: Que classe valida os dados de cada Utilizador (validação local)?
        +	Resposta: Utilizador
        +	Justificação: IE
    4.	Questão: Que classe valida a lista de Utilizadores (validação global)?
        +	Resposta: CentroDeExposicoes
        +	Justificação: IE
    4.	Questão: Que classe guarda o Utilizador?
        +	Resposta: RegistoUtilizador
        +	Justificação: IE. RegistoUtilizadores contém/agrega Utilizador(es)
5.	O sistema notifica o utilizador do sucesso da operação e apresenta os dados inseridos no sistema.
    5.	Questão: Que classe notifica?
        +	Resposta: RegistarUtilizadorUI
        +	Justificação: cf. 1.1
    5.	Questão: Que classe tem os dados para serem apresentados?
        +	Resposta: Utilizador
        +	Justificação: IE

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:


* Centro de Exposições  
* Utilizador  

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* RegistarUtilizadorUI  
* RegistarUtilizadorController 
* RegistoUtilizadores
## Diagrama de Sequência ##
![ds6.png](https://bitbucket.org/repo/BKrx4b/images/659026427-ds6.png)
## Diagrama de Classes ##
![dc6.png](https://bitbucket.org/repo/BKrx4b/images/3111569444-dc6.png)