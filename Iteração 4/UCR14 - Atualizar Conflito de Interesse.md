# Realização do UC14 Definir Recurso #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “AtualizarConflitoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**
![UC14-Racional.png](https://bitbucket.org/repo/BKrx4b/images/1464581410-UC14-Racional.png)![UC14-Racional.png](https://bitbucket.org/repo/BKrx4b/images/192507167-UC14-Racional.png)

## Diagrama de Sequência ##
![DS14-It3.jpg](https://bitbucket.org/repo/BKrx4b/images/1512991883-DS14-It3.jpg)
## Diagrama de Classes ##
![dc14.jpg](https://bitbucket.org/repo/4aRMAx/images/3805483839-dc14.jpg)