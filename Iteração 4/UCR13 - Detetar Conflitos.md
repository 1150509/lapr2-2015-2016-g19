# Realização do UC9 Definir Recurso #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “DefinirRecursoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**
![UC13-Racional.png](https://bitbucket.org/repo/BKrx4b/images/611509034-UC13-Racional.png)![UC13-Racional.png]
## Diagrama de Sequência ##
![DS13_1.png](https://bitbucket.org/repo/BKrx4b/images/821635996-DS13_1.png)
Interection use
![DS13_2.png](https://bitbucket.org/repo/BKrx4b/images/791445875-DS13_2.png)
![DS13_3.png](https://bitbucket.org/repo/BKrx4b/images/2096892430-DS13_3.png)
## Diagrama de Classes ##
![dc13.png](https://bitbucket.org/repo/BKrx4b/images/3034652136-dc13.png)