# Realização do UC1 Criar Exposição #

## Racional ##

**Quem coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “CriarExposiçãoController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O gestor de exposições inicia no sistema a criação de uma nova exposição. |... cria a nova Exposição? |RegistoExposicoes | Creator: Regra 1
2. O sistema solicita os dados necessários (título, texto descritivo, data início, data fim e local de realização). | n/a | | 
3. O gestor de exposições introduz os dados solicitados. | ... guarda os dados introduzidos? | Exposição |	Objeto criado no passo 1
4. O sistema mostra a lista de utilizadores do sistema que podem ser indicados como organizadores da exposição. | ... conhece todos os utilizadores existentes? | RegistoUtilizadores| 	IE: no DC RegistoUtilizador tem vários Utilizador
5. O gestor de exposições seleciona um utilizador como organizador. | n/a | | 
6. O sistema guarda o utilizador/organizador selecionado. | ... guarda o utilizador selecionado? |	Organizador |	IE:Um organizador é (corresponde) a um utilizador.
 | ... guarda o organizador criado?	| Exposição |IE: uma Exposição é organizada por vários organizadores.
7. Os passos 4 a 6 repetem-se até todos os utilizadores/organizadores da exposição estarem definidos. | | |
8. O sistema valida e apresenta todos os dados ao gestor de exposições, pedindo que os confirme. | ... valida os dados?	| Exposição | IE: Validação local: tem os seus dados (e.g. dados obrigatórios)
 | | RegistoExposicoes | IE: Validação global (e.g. duplicados)
9. O gestor de exposições confirma. | | | 
10. O sistema regista a nova exposição e informa o gestor de exposições do sucesso da operação. | ... guarda a exposição criada? | Centro de Exposições | IE: no MD o Centro de Exposições realiza várias Exposições 


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:


* Centro de Exposições  
* Exposição  
* Organizador  
* Utilizador  

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* CriarExposiçãoUI  
* CriarExposiçãoController 
* Registo Exposições
* Lista de Organizadores
* Registo Utilizadores
## Diagrama de Sequencia ##
![UC1-Ds_interection Use 1_3.jpg](https://bitbucket.org/repo/4aRMAx/images/2964553384-UC1-Ds_interection%20Use%201_3.jpg)

![UC1 Ds-interection use 2_3.jpg](https://bitbucket.org/repo/4aRMAx/images/3465579048-UC1%20Ds-interection%20use%202_3.jpg)

![Ds 1 Interection Use 3_3.jpg](https://bitbucket.org/repo/4aRMAx/images/4112152111-Ds%201%20Interection%20Use%203_3.jpg)


# Diagrama de Classes #

![dc1.jpg](https://bitbucket.org/repo/4aRMAx/images/4267513991-dc1.jpg)